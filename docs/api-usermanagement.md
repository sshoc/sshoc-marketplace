# Documentation of user stories with respect to user management

Collecting various user stories dealing with all parts of user management. Most of this will only work if you have proper permission.

## General information

There are currently four user roles:
* contributor
* system-contributor
* moderator
* administrator

People are able to register via EGI EOSC AAI, collecting various identity providers, e.g. DARIAH, eduTEAMS, ORCID, GitHub.
After registration, the new created account has the user role `contributor`. People with this user role can propose changes to existing items or propose to create new items (such items do have the status `SUGGESTED`). A `moderator` or an `administrator` is necessary to approve such suggestions. The user role `system-contributor` is used for (ingestion) scripts  to distinct from humans. Items from users with this role do have the status `INGESTED` (see: https://gitlab.gwdg.de/sshoc/sshoc-marketplace-backend/-/issues/68#note_297694).

## User management

### Get the ID of an user

This can only be performed by users with the user role `administrator`.

**API call:** GET `/api/users` (it is also possible to query for an user with the parameter `q`, searching in username, displayName and email)

**Result:** A list of users including the user ID (users[].id). Look out for the user where you like to get the id (or use `q`-parameter if you know the username).

## User role management

### Change the role of an user

This can only be performed by users with the user role `administrator`.

First get the ID of the user = {user-id} (see "User management => Get the ID of an user").
Then decide about the new user role for this user and take the value from the list of user roles = {user-role} (see "General information" resp. OpenAPI)

**API call:** PUT `/api/users/{user-id}/role/{user-role}

**Result:** If successful, `200 OK` and returning user information of {user-id} with the applied {user-role}. User will benefit immediately from the user role privileges.

See: https://gitlab.gwdg.de/sshoc/sshoc-marketplace-backend/-/issues/68#note_297534
