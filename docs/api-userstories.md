# Documentation of User Stories with respect to the API

This document collects various user stories (actions users need to be able to perform and what it means in terms of call to the Marketplace API.

## Managing sources

## Managing items

### Contributor suggesting a new item

**API calls:**
**Result:** New item stored in the DB with status: `SUGGESTED`

### Contributor suggesting a new workflow

Special provisions in case a workflow is being suggested.

**API calls:**
**Result:** 

### Contributor suggesting changes to an existing item

This includes also adding a new dynamic property to an existing item.

**API calls:**

**Result:** New version of an existing item stored in the DB with status: `SUGGESTED`

### Adding media type

`/api/media-sources`

