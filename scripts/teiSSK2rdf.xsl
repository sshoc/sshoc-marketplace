<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:tei="http://www.tei-c.org/ns/1.0"
  xmlns:my ="myFunction"
  exclude-result-prefixes="xs tei"
  version="2.0">
  <xsl:output encoding="UTF-8" method="text" indent="yes"/>
  
  <xsl:strip-space elements="*"/>
  
  <xsl:param name="entity-id" select="/tei:TEI/@xml:id"/>
  <xsl:variable name="scenariostep" select="lower-case(substring-after(/tei:TEI/@type,'research'))" />
  
  <xsl:template match="/">
    @prefix sshockh: &lt;https://vocabs.acdh.oeaw.ac.at/sshockh#>.
    @prefix acttype: &lt;https://vocabs.acdh.oeaw.ac.at/acttype#>.
    @prefix dct: &lt;http://purl.org/dc/terms/>.
    @prefix rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#>.
    @prefix dc: &lt;http://purl.org/dc/elements/1.1/>.
    @prefix skos: &lt;https://www.w3.org/2004/02/skos/core>.
            
    <xsl:apply-templates />
    
  </xsl:template>
  
  <xsl:template match="@*|node()" xmlns="http://www.tei-c.org/ns/1.0">
      <xsl:apply-templates select="@*|node()"/>
  </xsl:template>
  
  <xsl:template match="tei:TEI">
    <xsl:variable name="class" select="if($scenariostep='scenario') 
      then 'sshockh:Recipe' else 'sshockh:Step'" />
    <xsl:text>ssk:</xsl:text><xsl:value-of select="$entity-id"/> a <xsl:value-of select="$class"/>;
    <xsl:apply-templates />
    
  </xsl:template>
  
  <xsl:template match="tei:title" >  rdfs:label "<xsl:value-of select="."/>";
</xsl:template>
  
  
  <xsl:template match="tei:author" >
    <xsl:variable name="author-id" select="lower-case(concat(substring(tei:forename,1,1),tei:surname))" />
<xsl:text>    dct:creator ssk:</xsl:text><xsl:value-of select="$author-id"/> ;
</xsl:template>
  
  <xsl:template match="tei:term" xmlns="http://www.tei-c.org/ns/1.0">
       
    <xsl:variable name="termProperty"><xsl:choose>
         <xsl:when test="@type='discipline'">sshockh:applicableInDiscipline</xsl:when>      
      <xsl:when test="@type='object'">sshockh:applicableForInformationObjectType</xsl:when>
    <xsl:when test="@type='technique' or @type='activity'">sshockh:applicableForInformationObjectType</xsl:when>      <xsl:when test="@type='standard'">sshockh:applicableStandard</xsl:when>
       </xsl:choose>
    </xsl:variable>
    
    <xsl:value-of select="concat('   ',$termProperty, ' ', my:sanitize(concat(@source,':',@key,.)))"/><xsl:text>;
</xsl:text>
      
  </xsl:template>
  
  <xsl:template match="tei:event" xmlns="http://www.tei-c.org/ns/1.0">
 <xsl:text>   sshockh:hasStep ssk:</xsl:text><xsl:value-of select="@ref"/>;
<xsl:apply-templates select="@*|node()" />
</xsl:template>
  
  <xsl:template match="/tei:TEI/tei:text/tei:body/tei:listEvent/tei:event/tei:head" xmlns="http://www.tei-c.org/ns/1.0">
    <xsl:copy>
      <xsl:attribute name="type">stepTitle</xsl:attribute>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:desc/@corresp"/>
  
  <xsl:template match="tei:desc[@corresp='definition']" xmlns="http://www.tei-c.org/ns/1.0">
    <xsl:copy>
      <xsl:attribute name="type">definition</xsl:attribute>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>
  
 
  
  <xsl:template match="tei:linkGrp">
    <xsl:choose>
      <xsl:when test="@corresp='generalResources'">
        <xsl:copy>
          <xsl:attribute name="type">generalResources</xsl:attribute>
          <xsl:attribute name="xml:base">http://www.zotero.org/groups/427927/ssk-parthenos/items/itemKey/</xsl:attribute>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:when>
      <xsl:when test="@corresp='projectResources'">
        <xsl:copy>
          <xsl:attribute name="type">projectResources</xsl:attribute>
          <xsl:attribute name="corresp"><xsl:value-of select="@url"/></xsl:attribute>
          <xsl:attribute name="xml:base">http://www.zotero.org/groups/427927/ssk-parthenos/items/itemKey/</xsl:attribute>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  
  
  <xsl:template match="tei:figure/@corresp"/>
  <xsl:template match="tei:head/@corresp"/>
  <xsl:template match="tei:author/@corresp"/>
  <xsl:template match="tei:linkGrp/@corresp"/>
  <xsl:template match="tei:linkGrp/@url"/>
  
  <xsl:template match="tei:lb"/>
  <xsl:template match="@default"/>
  <xsl:template match="@part"/>
  <xsl:template match="@instant"/>
  <xsl:template match="@full"/>
  <xsl:template match="@org"/>
  <xsl:template match="@sample"/>
  
  <xsl:function name="my:sanitize" >
    <xsl:param name="value"></xsl:param>
    
    <xsl:value-of select="lower-case(translate($value,' ', '_'))"/>    
    
  </xsl:function>
  
</xsl:stylesheet>